import { Component, OnInit } from '@angular/core';
import { Template } from '../models/template.model';
import { TemplatesService } from '../services/templates.service';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-templates',
  templateUrl: './templates.component.html',
  styleUrls: ['./templates.component.css']
})
export class TemplatesComponent implements OnInit {

  public templatesWithPicture: Template[] = [];
  public templates: Template[] = [];
  public pictures: String[] = []; 

  constructor(private templateService: TemplatesService,private authService: AuthService) {
    this.templatesWithPicture = this.templateService.getTemplatesWP();
    this.templates = this.templateService.getTemplates();
  }

  ngOnInit() {
    this.authService.updateTokenExpiry('auth-token');
  }

}
