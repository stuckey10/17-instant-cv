const router = require('express').Router();


const controller = require('../../controllers/userController');

//api/user/

router.post('/register', controller.registerFunc);

router.post('/register/validation', controller.validationFunc);

router.post('/login', controller.loginFunc);

router.get('/:userID', controller.userInfo);

router.get('/img/:imageName', controller.profilePic);


module.exports = router;